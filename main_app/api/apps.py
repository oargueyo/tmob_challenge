from django.apps import AppConfig
from django.db.models.signals import post_save, post_delete


class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        from .models import Redirect
        from .handlers import redirect_post_save_handler,redirect_post_delete_handler
        post_save.connect(redirect_post_save_handler, sender=Redirect)
        post_delete.connect(redirect_post_delete_handler, sender=Redirect)
