from django.core.cache import cache


def _invalidate_cached_data(pk):
    cache_key = f"redirect_{pk}"
    cache.delete(cache_key)


def redirect_post_save_handler(sender, instance, created, **kwargs):
    if not created:
        _invalidate_cached_data(instance.key)


def redirect_post_delete_handler(sender, instance, **kwargs):
    _invalidate_cached_data(instance.key)