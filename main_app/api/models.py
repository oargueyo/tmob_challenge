from django.db import models
from django.utils.translation import ugettext as translate
from django.utils.timezone import now

# Create your models here.
class ToggleableMixin(models.Model):
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class AutoCreatedUpdatedMixin(models.Model):
    created_at = models.DateTimeField(
        verbose_name=translate('created at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )

    updated_at = models.DateTimeField(
        verbose_name=translate('updated at'),
        unique=False,
        null=True,
        blank=True,
        db_index=True,
    )

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.id or not self.created_at:
            self.created_at = now()
            self.updated_at = self.created_at
        else:
            auto_updated_at_is_disabled = kwargs.pop('disable_auto_updated_at', False)
            if not auto_updated_at_is_disabled:
                self.updated_at = now()
        super(AutoCreatedUpdatedMixin, self).save(*args, **kwargs)


class Redirect(AutoCreatedUpdatedMixin , ToggleableMixin):

    key = models.SlugField(max_length=200, unique=True)
    url = models.URLField()

    class Meta:
        ordering = ['key']

    def __str__(self):
        return f"{self.key}: {self.url}"

    @staticmethod
    def get_redirect(key):
        return Redirect.objects.filter(key=key)

