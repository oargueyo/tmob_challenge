from django.shortcuts import render
from django.core.cache import cache
from django.shortcuts import get_object_or_404

from rest_framework import viewsets

from .serializers import RedirectSerializer
from .models import Redirect


class RetrieveCachedInstanceObjectMixin:

    def get_object(self):
        filter_kwargs = {self.lookup_field: self.kwargs[self.lookup_url_kwarg]}
        cache_key = f"redirect_{filter_kwargs.get(self.lookup_field)}"
        data = cache.get(cache_key)
        if data:
            return data

        # Podriamos user directamente el super.get_object() de ModelViewSet que filtrara por key debido al lookup_field,
        # pero para el proposito de esta prueba se uso la funcion get_redirect que filtra los redirect by key.

        instance = get_object_or_404(self.model_class.get_redirect(
            key=filter_kwargs.get(self.lookup_field)
        )
            ,active=True
        )
        #instance = super().get_object()
        if instance:
            cache.set(cache_key, instance)
        return instance


class RedirectViewSet(RetrieveCachedInstanceObjectMixin, viewsets.ModelViewSet):

    queryset = Redirect.objects.filter(active=True)
    serializer_class = RedirectSerializer
    model_class = Redirect
    lookup_url_kwarg = 'key'
    lookup_field = 'key'

